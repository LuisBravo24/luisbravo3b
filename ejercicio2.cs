﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ejercicio2._0
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("ingrese el tamaño que desea:");
            int T = Convert.ToInt32(Console.ReadLine() );
            if (T>=0 && T<=50)
            {
                for (int i = 0; i < T; i++)
                {
                    Console.Write(" *");
                }
                Console.WriteLine(" ");
                for (int x = 0; x < T-2 ; x++)
                {
                    Console.Write(" *");
                    for (int y = 0; y < T-2 ; y++)
                    {
                        Console.Write("  ");
                    }
                    Console.WriteLine(" *");
                }
                for (int z = 0; z < T; z++)
                {
                    Console.Write(" *");
                }
            }
            Console.ReadKey();
        }
    }
}
